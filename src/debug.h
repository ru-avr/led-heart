/*
 *  debug.h
 *
 *  Created on: 2021-02-06
 *  Author: rafau
 */

#include <stdint.h>

#include "circular_buffer.h"

#define DEBUG_RX_BUFFER_SIZE 256
#define DEBUG_TX_BUFFER_SIZE 256

typedef enum debug_cmd_error {
    debug_cmd_error_SUCCESS = 0,
    debug_cmd_error_NODRV,
    debug_cmd_error_EMPTY,
    debug_cmd_error_INVCMD,
    debug_cmd_error_ARGERR,
    debug_cmd_error_FAIL,
    _debug_cmd_error_SIZE,
} debug_cmd_error_t;

struct debug_driver {
    int32_t (*usart_write_func)(char *, uint16_t);
    uint16_t (*uptime_func)(void);
    struct circular_buffer *rx_buffer;
    struct circular_buffer *tx_buffer;
    volatile uint8_t rx_lines_cnt;
    volatile uint8_t tx_busy;
};

typedef debug_cmd_error_t(debug_command_func)(int argc, char *argv[]);
struct debug_command {
    char *name;
    char *description;
    char *desc_args;
    debug_command_func *func;
};

uint8_t debug_init(struct debug_driver *driver);
int32_t debug_printf(const char *module, const char *format, ...);

uint8_t debug_command_register(struct debug_command *cmd);
debug_cmd_error_t debug_command_handler(void);

void debug_rx_interrupt_handler(uint8_t byte);
void debug_tx_interrupt_handler(uint8_t *byte);

const char *debug_cmd_error_strerr(debug_cmd_error_t err);