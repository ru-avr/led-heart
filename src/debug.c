/*
 *  debug.c
 *
 *  Created on: 2021-02-06
 *  Author: rafau
 */

#include "debug.h"

#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "info.h"
#include "utils.h"

#define UINT16_MAX_DIGITS_NUM 5
#define DEBUG_COMMANDS_SIZE 8
#define DEBUG_COMMAND_ARGV_SIZE 32

#define LOG_DEBUG(format, ...) debug_printf(__FILE__, format, ##__VA_ARGS__)

static struct debug_driver *_driver = NULL;

struct debug_command *_commands[DEBUG_COMMANDS_SIZE] = {0};

static debug_cmd_error_t _on_cmd_hello(int argc, char *argv[]);
static debug_cmd_error_t _on_cmd_help(int argc, char *argv[]);

static struct debug_command _cmd_hello = {
    .name = "hello", .description = "Prints greetings and version", .func = _on_cmd_hello};
static struct debug_command _cmd_help = {.name = "help", .description = "Prints help", .func = _on_cmd_help};

uint8_t debug_init(struct debug_driver *driver) {
#ifdef DEBUG
    if (!driver || !driver->rx_buffer || !driver->tx_buffer)
        return 1;

    if (circular_buffer_clear(driver->rx_buffer))
        return 2;

    if (circular_buffer_clear(driver->tx_buffer))
        return 3;

    _driver = driver;
    _driver->rx_lines_cnt = 0;
    _driver->tx_busy = 0;

    uint8_t rc;
    rc = debug_command_register(&_cmd_hello);
    if (rc)
        return 4;

    rc = debug_command_register(&_cmd_help);
    if (rc)
        return 5;

#endif
    return 0;
}

char *_uint16_to_str(uint16_t i) {
    static char buffer[UINT16_MAX_DIGITS_NUM];
    char *p = buffer + UINT16_MAX_DIGITS_NUM;

    memset(buffer, '0', sizeof(buffer));

    do {
        *(--p) = (i % 10) + '0';
        i = i / 10;
    } while (i);

    return buffer;
}

static struct debug_command *_find_command(const char *name) {
    if (!name)
        return NULL;

    struct debug_command *cmd;

    for (uint8_t i = 0; i < DEBUG_COMMANDS_SIZE; i++) {
        cmd = _commands[i];
        if (!cmd)
            continue;

        if (strcmp(name, cmd->name) == 0)
            return cmd;
    }

    return NULL;
}

int32_t debug_printf(const char *module, const char *format, ...) {
#ifdef DEBUG
    static char buffer[DEBUG_TX_BUFFER_SIZE] = {0};

    if (!_driver->usart_write_func)
        return -1;

    uint16_t offset = 0;
    if (_driver->uptime_func) {
        uint16_t now = _driver->uptime_func();
        memcpy(buffer, _uint16_to_str(now), UINT16_MAX_DIGITS_NUM);
        offset += UINT16_MAX_DIGITS_NUM;
    }

    buffer[offset++] = ' ';
    buffer[offset++] = '[';

    uint16_t module_len = strlen(module);
    if (module_len < 32) {
        strncpy(buffer + offset, module, module_len);
        offset += module_len;
    }

    buffer[offset++] = ']';
    buffer[offset++] = ' ';

    size_t remaining_size = DEBUG_TX_BUFFER_SIZE - offset - 2; // 2 = sizeof "\n\r"

    va_list args;
    int32_t rc;

    va_start(args, format);
    rc = vsnprintf(buffer + offset, remaining_size, format, args);
    va_end(args);

    if (rc <= 0)
        return rc;

    offset += rc;

    buffer[offset++] = '\n';
    buffer[offset++] = '\r';

    return _driver->usart_write_func(buffer, offset);

#endif
    return 0;
}

uint8_t debug_command_register(struct debug_command *cmd) {
    for (uint8_t i = 0; i < DEBUG_COMMANDS_SIZE; i++) {
        if (!_commands[i]) {
            _commands[i] = cmd;
            return 0;
        }
    }
    return 1;
}

debug_cmd_error_t debug_command_handler(void) {
    static uint8_t command_buffer[DEBUG_RX_BUFFER_SIZE];

    if (!_driver)
        return debug_cmd_error_NODRV;

    if (!_driver->rx_lines_cnt)
        return debug_cmd_error_SUCCESS;

    int16_t index = 0;
    while (!circular_buffer_empty(_driver->rx_buffer)) {
        uint8_t byte = circular_buffer_pop(_driver->rx_buffer);
        command_buffer[index++] = byte;
        if (byte == '\0')
            break;
    }
    _driver->rx_lines_cnt--;

    if (command_buffer[0] == '\0')
        return debug_cmd_error_EMPTY;

    LOG_DEBUG("Command received %s", command_buffer);

    int argc = 0;
    char *argv[DEBUG_COMMAND_ARGV_SIZE] = {0};

    char *aux_ptr = strtok((char *)command_buffer, " \t");

    while (aux_ptr) {
        if (argc < DEBUG_COMMAND_ARGV_SIZE)
            argv[argc++] = aux_ptr;

        aux_ptr = strtok(NULL, " \t");
    }

    struct debug_command *cmd = _find_command(argv[0]);
    if (!cmd || !cmd->func) {
        return debug_cmd_error_INVCMD;
    }

    debug_cmd_error_t rc = cmd->func(argc, argv);

    return rc;
}

void debug_rx_interrupt_handler(uint8_t byte) {
    if (byte == '\r') {
        _driver->rx_lines_cnt++;
        byte = '\0';
    }

    // NOTE: Could fail but nothing could be done
    circular_buffer_push(_driver->rx_buffer, byte);
}

void debug_tx_interrupt_handler(uint8_t *byte) {
    if (circular_buffer_empty(_driver->tx_buffer)) {
        _driver->tx_busy = 0;
    }

    *byte = circular_buffer_pop(_driver->tx_buffer);
    _driver->tx_busy = 1;
}

const char *debug_cmd_error_strerr(debug_cmd_error_t err) {
    static char *strerr[] = {"SUCCESS",         "Driver not initialized",   "Empty command",
                             "Invalid command", "Invalid command argument", "Command execution failed"};

    if (err >= ARRAY_SIZE(strerr))
        return "undefined";

    return strerr[err];
}

static debug_cmd_error_t _on_cmd_hello(int argc, char *argv[]) {
    LOG_DEBUG("Hello!");
    LOG_DEBUG("Firmware version: %s", FIRMWARE_VERSION);
    return debug_cmd_error_SUCCESS;
}

static debug_cmd_error_t _on_cmd_help(int argc, char *argv[]) {
    LOG_DEBUG("Help:");

    struct debug_command *aux;

    for (uint8_t i = 0; i < ARRAY_SIZE(_commands); i++) {
        aux = _commands[i];
        if (!aux)
            continue;

        if (aux->desc_args) {
            LOG_DEBUG("\t%d. %s %s - %s", i, aux->name, aux->desc_args, aux->description);
        } else {
            LOG_DEBUG("\t%d. %s - %s", i, aux->name, aux->description);
        }
    }

    return debug_cmd_error_SUCCESS;
}