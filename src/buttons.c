/*
 *  buttons.c
 *
 *  Created on: 2021-02-06
 *  Author: rafau
 */

#include "buttons.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <util/delay.h>

#include "debug.h"

// ru_avrlib
#include "ru_avrlib/atxmega/rtc.h"

#define LOG_DEBUG(format, ...) debug_printf(__FILE__, format, ##__VA_ARGS__)

#define DEBOUNCING_BUFFER_SIZE 4

struct {
    volatile uint8_t states[DEBOUNCING_BUFFER_SIZE];
    volatile uint16_t checks_counter;
} _buttons;

void buttons_init(void) {
    // PC4 - SW4, PC5 - SW3, PC6 - SW2, PC7 - SW1

    PORTC.DIR &= ~(PIN4_bm | PIN5_bm | PIN6_bm | PIN7_bm); // PC4 - PC7 as inputs

    PORTC.REMAP = 0;

    PORTC.PIN4CTRL |= PORT_INVEN_bm | PORT_OPC_WIREDANDPULL_gc;
    PORTC.PIN5CTRL |= PORT_INVEN_bm | PORT_OPC_WIREDANDPULL_gc;
    PORTC.PIN6CTRL |= PORT_INVEN_bm | PORT_OPC_WIREDANDPULL_gc;
    PORTC.PIN7CTRL |= PORT_INVEN_bm | PORT_OPC_WIREDANDPULL_gc;
}

uint8_t buttons_state(void) {
    uint8_t state = 0xFF;

    for (size_t i = 0; i < DEBOUNCING_BUFFER_SIZE;) {
        state &= _buttons.states[i++];
    }

    return state;
}

uint16_t buttons_check_per_second(void) {
    static uint16_t last_query_time = 0;

    uint16_t now = uptime();
    uint16_t c = _buttons.checks_counter / (now - last_query_time);

    last_query_time = now;
    _buttons.checks_counter = 0;

    return c;
}

void buttons_timer_interrupt_handler(void) {
    static uint8_t pointer = 0;

    _buttons.states[pointer++] = (PORTC.IN >> 4);
    _buttons.checks_counter++;

    pointer %= DEBOUNCING_BUFFER_SIZE;
}