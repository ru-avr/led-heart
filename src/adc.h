/*
 *  adc.h
 *
 *  Created on: 2021-02-05
 *  Author: rafau
 */

#ifndef __ADC_H__
#define __ADC_H__

#include <stdint.h>

void adc_init(uint8_t interrupt_level);

uint16_t adc_ch0(void);

#endif /* !__ADC_H__ */
