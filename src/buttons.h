/*
 *  buttons.h
 *
 *  Created on: 2021-02-06
 *  Author: rafau
 */

#ifndef __BUTTONS_H__
#define __BUTTONS_H__

#include <stdint.h>

void buttons_init(void);

uint8_t buttons_state(void);

uint16_t buttons_check_per_second(void);

void buttons_timer_interrupt_handler(void);

#endif /* !__BUTTONS_H__ */
