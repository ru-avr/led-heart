/*
 *  circular_buffer.h
 *
 *  Created on: 2023-01-28
 *  Author: rafau
 */
#include <stdint.h>
#include <stdio.h>

#ifndef __CIRCULAR_BUFFER_H__
#define __CIRCULAR_BUFFER_H__

struct circular_buffer {
    uint8_t *buffer;
    uint16_t size;
    uint16_t head;
    uint16_t tail;
    uint16_t count;
};

uint8_t circular_buffer_clear(struct circular_buffer *cb);
uint8_t circular_buffer_push(struct circular_buffer *cb, uint8_t byte);
uint8_t circular_buffer_empty(struct circular_buffer *cb);
uint8_t circular_buffer_pop(struct circular_buffer *cb);

#endif /* !__CIRCULAR_BUFFER_H__ */