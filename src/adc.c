/*
 *  adc.c
 *
 *  Created on: 2021-02-05
 *  Author: rafau
 */

#include "adc.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#include "debug.h"

// ru_avrlib
#include "ru_avrlib/atxmega/usart.h"

#define LOG_DEBUG(format, ...) debug_printf(__FILE__, format, ##__VA_ARGS__)

static volatile uint16_t _adc_ch0 = 0;

void adc_init(uint8_t interrupt_level) {
    PORTA.DIR &= ~(1 << PIN0);  // PA0 - input

    ADCA.CTRLA = ADC_ENABLE_bm;
    ADCA.REFCTRL = ADC_REFSEL_INTVCC_gc;
    ADCA.CTRLB |= ADC_RESOLUTION_12BIT_gc | ADC_CURRLIMIT_MED_gc | ADC_FREERUN_bm;
    ADCA.PRESCALER = ADC_PRESCALER_DIV512_gc;

    interrupt_level &= (0x03 << 0);

    ADCA.CH0.CTRL |= ADC_CH_INPUTMODE_SINGLEENDED_gc;
    ADCA.CH0.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | interrupt_level;
    ADCA.CH0.CTRL |= ADC_CH_START_bm;

    LOG_DEBUG("ADC initialized");
}

uint16_t adc_ch0(void) {
    return _adc_ch0;
}

ISR(ADCA_CH0_vect) {
    _adc_ch0 = ADCA.CH0RES;
}
