/*
 *  leds.h
 *
 *  Created on: 2021-02-05
 *  Author: rafau
 */

#ifndef __LEDS_H__
#define __LEDS_H__

#include <stdint.h>

typedef enum leds_sequence {
    leds_sequence_init = 0,
    leds_sequence_manual,
    leds_sequence_foo,
    leds_sequence_bar,
    leds_sequence_baz,
    leds_sequence_qux,
    _leds_sequence_size,
} leds_sequence_t;

uint8_t leds_init(uint8_t twi_interrupt_level);

uint8_t leds_set_sequence(leds_sequence_t sequence);

uint8_t leds_manual_set_value(uint16_t value);

uint8_t leds_worker(void);

void leds_timer_interrupt_handler(void);

const char *leds_sequence_name(void);

#endif /* !__LEDS_H__ */
