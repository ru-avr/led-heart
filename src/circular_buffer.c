/*
 *  circular_buffer.c
 *
 *  Created on: 2023-01-28
 *  Author: rafau
 */
#include "circular_buffer.h"

uint8_t circular_buffer_clear(struct circular_buffer *cb) {
    if (!cb)
        return 1;

    cb->head = 0;
    cb->tail = 0;
    cb->count = 0;

    return 0;
}

uint8_t circular_buffer_push(struct circular_buffer *cb, uint8_t byte) {
    if (cb->count == cb->size) {
        return 0;
    }

    cb->buffer[cb->tail] = byte;
    cb->tail = (cb->tail + 1) % cb->size;
    cb->count++;

    return 1;
}

uint8_t circular_buffer_empty(struct circular_buffer *cb) { return cb->count == 0; }

uint8_t circular_buffer_pop(struct circular_buffer *cb) {
    if (circular_buffer_empty(cb)) {
        return -1;
    }

    uint8_t byte = cb->buffer[cb->head];
    cb->head = (cb->head + 1) % cb->size;
    cb->count--;

    return byte;
}