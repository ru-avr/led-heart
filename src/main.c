#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <stdint.h>
#include <util/delay.h>

#include "adc.h"
#include "buttons.h"
#include "circular_buffer.h"
#include "debug.h"
#include "leds.h"
#include "utils.h"

// ru_avrlib
#include "ru_avrlib/atxmega/rtc.h"
#include "ru_avrlib/atxmega/usart.h"

#define LOG_DEBUG(format, ...) debug_printf(__FILE__, format, ##__VA_ARGS__)

int32_t debug_write_func(char *s, uint16_t len) { return usart_write(&USARTC0, s, len); }

void interrupts_init(void) {
    PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;

    sei();
}

void timer4_init(uint8_t ovf_interrupt_level) {
    TCC4.CTRLA = TC45_CLKSEL_DIV256_gc | (0 << TC4_SYNCHEN_bp);
    TCC4.CTRLB = TC45_WGMODE_NORMAL_gc | TC45_CIRCEN_DISABLE_gc | TC45_BYTEM_NORMAL_gc;
    // TCC4.CTRLC = (0 << TC4_POLD_bp) | (0 << TC4_POLC_bp) | (0 << TC4_POLB_bp)
    // | (0 << TC4_POLA_bp) | (0 << TC4_CMPD_bp) |
    //              (0 << TC4_CMPC_bp) | (0 << TC4_CMPB_bp) | (0 <<
    //              TC4_CMPA_bp);
    // TCC4.CTRLD = TC45_EVACT_OFF_gc | TC45_EVSEL_OFF_gc;
    // TCC4.CTRLE = TC45_CCDMODE_DISABLE_gc | TC45_CCCMODE_DISABLE_gc |
    // TC45_CCBMODE_DISABLE_gc | TC45_CCAMODE_DISABLE_gc;

    ovf_interrupt_level &= (0x03 << 0);

    TCC4.INTCTRLA = TC45_ERRINTLVL_OFF_gc | ovf_interrupt_level;
    TCC4.INTCTRLB = TC45_CCDINTLVL_OFF_gc | TC45_CCCINTLVL_OFF_gc | TC45_CCBINTLVL_OFF_gc | TC45_CCAINTLVL_OFF_gc;

    TCC4.CNT = 0x0000;

    TCC4.PER = 0xFF;

    TCC4.CCA = 0x0000;
    TCC4.CCB = 0x0000;
    TCC4.CCC = 0x0000;
    TCC4.CCD = 0x0000;
}

void timer5_init(uint8_t ovf_interrupt_level) {
    TCC5.CTRLA = TC45_CLKSEL_DIV256_gc | (0 << TC4_SYNCHEN_bp);
    TCC5.CTRLB = TC45_WGMODE_NORMAL_gc | TC45_CIRCEN_DISABLE_gc | TC45_BYTEM_NORMAL_gc;
    // TCC5.CTRLC = (0 << TC4_POLD_bp) | (0 << TC4_POLC_bp) | (0 << TC4_POLB_bp)
    // | (0 << TC4_POLA_bp) | (0 << TC4_CMPD_bp) |
    //              (0 << TC4_CMPC_bp) | (0 << TC4_CMPB_bp) | (0 <<
    //              TC4_CMPA_bp);
    // TCC5.CTRLD = TC45_EVACT_OFF_gc | TC45_EVSEL_OFF_gc;
    // TCC5.CTRLE = TC45_CCDMODE_DISABLE_gc | TC45_CCCMODE_DISABLE_gc |
    // TC45_CCBMODE_DISABLE_gc | TC45_CCAMODE_DISABLE_gc;

    ovf_interrupt_level &= (0x03 << 0);

    TCC5.INTCTRLA = TC45_ERRINTLVL_OFF_gc | ovf_interrupt_level;
    TCC5.INTCTRLB = TC45_CCDINTLVL_OFF_gc | TC45_CCCINTLVL_OFF_gc | TC45_CCBINTLVL_OFF_gc | TC45_CCAINTLVL_OFF_gc;

    TCC5.CNT = 0x0000;

    TCC5.PER = 0xFF;

    TCC5.CCA = 0x0000;
    TCC5.CCB = 0x0000;
}

static uint8_t usart_rx_buf[DEBUG_RX_BUFFER_SIZE];
static uint8_t usart_tx_buf[DEBUG_TX_BUFFER_SIZE];

static struct circular_buffer usart_rx_cb = {.buffer = usart_rx_buf, .size = ARRAY_SIZE(usart_rx_buf)};
static struct circular_buffer usart_tx_cb = {.buffer = usart_tx_buf, .size = ARRAY_SIZE(usart_tx_buf)};

static struct debug_driver debug_drv = {
    .usart_write_func = debug_write_func,
    .uptime_func = uptime,
    .rx_buffer = &usart_rx_cb,
    .tx_buffer = &usart_tx_cb,
};

int main(void) {
    uint8_t rc;

    uint16_t now = 0;
    uint16_t last_log_send = 0;
    uint16_t last_buttons_handle = 0;
    debug_cmd_error_t debug_cmd_rc;

    rtc_init(CLK_RTCSRC_RCOSC_gc);
    interrupts_init();

    wdt_enable(WDT_PER_8KCLK_gc | WDT_ENABLE_bm | WDT_CEN_bm);

    timer4_init(TC45_OVFINTLVL_LO_gc);
    timer5_init(TC45_OVFINTLVL_MED_gc);

    usart_gpio_init(&PORTC, PIN2, PIN3);
    usart_init(&USARTC0, USART_RXCINTLVL_LO_gc, USART_TXCINTLVL_LO_gc);
    // usart_init(&USARTC0, USART_RXCINTLVL_OFF_gc, USART_TXCINTLVL_OFF_gc);
    usart_set_baudrate(&USARTC0, F_CPU, 115200);

    rc = debug_init(&debug_drv);
    if (rc)
        goto debug_fault;

    rc = leds_init(TWI_SLAVE_INTLVL_HI_gc);
    if (rc)
        goto fault;

    buttons_init();

    // adc_init(ADC_CH_INTLVL_MED_gc);

    leds_set_sequence(leds_sequence_manual);
    leds_manual_set_value(0xAAAA);

    while (1) {
        wdt_reset();

        now = uptime();
        if (now > last_log_send) {
            // LOG_DEBUG("ADC CH0 = %05u", adc_ch0());
            // LOG_DEBUG("leds sequence: %s", leds_sequence_name());
            // LOG_DEBUG("tick...");

            last_log_send = now;
        }

        debug_cmd_rc = debug_command_handler();
        if (debug_cmd_rc != debug_cmd_error_SUCCESS) {
            LOG_DEBUG("Last command error: %s", debug_cmd_error_strerr(debug_cmd_rc));
        }

        // if (now > last_buttons_handle) {
        //     LOG_DEBUG("switches checks per second: %u", buttons_check_per_second());
        // }

        // uint8_t sw_state = buttons_state();

        // // TODO; FIX SW2 button!
        // sw_state &= ~0x04;

        // switch (sw_state) {
        // case 0x01:
        //     leds_set_sequence(leds_sequence_foo);
        //     break;
        // case 0x02:
        //     leds_set_sequence(leds_sequence_bar);
        //     break;
        // case 0x04:
        //     leds_set_sequence(leds_sequence_baz);
        //     break;
        // case 0x08:
        //     leds_set_sequence(leds_sequence_fck);
        //     break;
        // }

        // last_buttons_handle = now;

        rc = leds_worker();
        if (rc)
            goto fault;
    }

fault:
    LOG_DEBUG("Something goes wrong");
debug_fault:

    while (1) {
    }

    return 0;
}

ISR(TCC4_OVF_vect) {
    if (!(TCC4.INTFLAGS & TC4_OVFIF_bm))
        return;
    TCC4.INTFLAGS |= TC4_OVFIF_bm;

    leds_timer_interrupt_handler();

    TCC4.PER = adc_ch0();
}

ISR(TCC5_OVF_vect) {
    if (!(TCC5.INTFLAGS & TC5_OVFIF_bm))
        return;
    TCC5.INTFLAGS |= TC5_OVFIF_bm;

    buttons_timer_interrupt_handler();
}

ISR(USARTC0_RXC_vect) {
    uint8_t byte = USARTC0_DATA;
    debug_rx_interrupt_handler(byte);
}

ISR(USARTC0_TXC_vect) { debug_tx_interrupt_handler(&USARTC0_DATA); }