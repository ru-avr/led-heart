/*
 *  utils.h
 *
 *  Created on: 2021-02-05
 *  Author: rafau
 */

#ifndef __UTILS_H__
#define __UTILS_H__

// #define ARRAY_SIZE(x) ((sizeof x) / (sizeof *x))
#define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))

#endif /* !__UTILS_H__ */