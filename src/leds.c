/*
 *  leds.c
 *
 *  Created on: 2021-02-05
 *  Author: rafau
 */

#include "leds.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdlib.h>
#include <util/delay.h>

#include "adc.h"
#include "debug.h"
#include "utils.h"

// ru_avrlib
#include "ru_avrlib/atxmega/rtc.h"
#include "ru_avrlib/atxmega/twi_master.h"
#include "ru_avrlib/atxmega/usart.h"

// ru_deviceslib
#include "ru_deviceslib/gpio_expander/mcp23017.h"

#define LEDS_NUM 16

#define LOG_DEBUG(format, ...) debug_printf(__FILE__, format, ##__VA_ARGS__)

static twi_master_t _twi = {};

static uint8_t _mcp23017_reset_pin_set(uint8_t value);
static uint8_t _mcp23017_i2c_write_data(uint8_t address, uint8_t *data, uint8_t size);
static uint8_t _mcp23017_delay_ms(uint32_t ms);

static debug_cmd_error_t _on_cmd_led_set(int argc, char *argv[]);

struct mcp23017_driver _mcp23017_drv = {
    .rst_pin_set_func = _mcp23017_reset_pin_set,
    .i2c_write_data = _mcp23017_i2c_write_data,
    .delay_ms_func = _mcp23017_delay_ms,
};

struct mcp23017_device _led_expander = {
    .driver = &_mcp23017_drv,
};

static struct debug_command _cmd_set_led = {
    .name = "set_led", .description = "Sets LEDs sequence", .desc_args = "<sequence num>", .func = _on_cmd_led_set};

static struct {
    leds_sequence_t current_worker;
    uint8_t (*worker_func[_leds_sequence_size])();

    volatile uint8_t timer_event;

    uint16_t value;
    uint16_t last_worker_run;
    uint16_t time_event_cnt;
} _leds;

static void _mcp23017_log_error(const char *context, uint8_t err) {
#ifdef DEBUG
    LOG_DEBUG("MCP23017: %s: %s", context, mcp23017_strerr(err));
#endif
}

static void _twi_log_error(const char *context, uint8_t err) {
#ifdef DEBUG
    LOG_DEBUG("TWI: %s: %s", context, twi_master_strerr(err));
#endif
}

static uint8_t _leds_set(uint16_t value) {
    uint8_t rc = 0;

    // LOG_INFO("leds set %04X", value);

    rc = mcp23017_set_value_for_all(&_led_expander, value);
    if (rc) {
        _mcp23017_log_error("mcp23017 set value for all error:", rc);
        return rc;
    }

    _leds.value = value;

    return rc;
}

static uint8_t _sequence_init_worker() {
    const uint16_t default_value = 0xAAAA;

    uint8_t rc = 0;

    if (default_value != _leds.value && (~default_value) != _leds.value)
        _leds.value = default_value;

    rc = _leds_set(_leds.value);
    if (rc)
        return rc;

    _leds.value = ~_leds.value;

    return rc;
}

static uint8_t _sequence_manual_worker() {
    const uint16_t default_value = 0xAAAA;

    uint8_t rc = 0;

    if (default_value != _leds.value && (~default_value) != _leds.value)
        _leds.value = default_value;

    rc = _leds_set(_leds.value);
    if (rc)
        return rc;

    _leds.value = ~_leds.value;

    return rc;
}

static uint8_t _sequence_foo_worker() {
    static uint8_t position = 0;
    static uint8_t inc = 1;

    uint8_t rc = _leds_set((1 << position));
    if (rc)
        return rc;

    // LOG_DEBUG("inc = %i, position = %u", inc, position);

    position = inc ? position + 1 : position - 1;
    position %= LEDS_NUM;

    if (position == LEDS_NUM - 1)
        inc = !inc;

    return rc;
}

static uint8_t _sequence_bar_worker() {
    static uint8_t position = 0;
    static uint8_t leds = 0x05;

    uint8_t rc = _leds_set((leds << position));
    if (rc)
        return rc;

    // LOG_DEBUG("leds = %u, position = %u", leds, position);

    ++position;
    position %= LEDS_NUM;

    return rc;
}

static uint8_t _sequence_baz_worker() {
    static uint8_t position = 15;
    static uint16_t leds = 0x00;
    static uint8_t inc = 1;

    uint8_t rc = 0;
    rc = _leds_set(leds | 0x8000);
    if (rc)
        return rc;

    if (inc) {
        leds |= (1 << position);
    } else {
        leds &= ~(1 << position);
    }

    ++position;
    position %= (LEDS_NUM);

    if (position == LEDS_NUM - 1)
        inc = !inc;

    return rc;
}

static uint8_t _sequence_fck_worker() {
    static uint8_t position = 0;
    const uint16_t sequence[] = {
        0x8000, 0x4001, 0x2002, 0x1004, 0x0808, 0x0410, 0x0220, 0x0140,
        0x0080, 0x0140, 0x0220, 0x0410, 0x0808, 0x1004, 0x2002, 0x4001,
    };

    uint8_t rc = 0;
    rc = _leds_set((sequence[position]));
    if (rc)
        return rc;

    // LOG_DEBUG("position = %u", position);

    ++position;
    position %= ARRAY_SIZE(sequence);

    return rc;
}

uint8_t leds_init(uint8_t twi_interrupt_level) {
    uint8_t rc;

    // set mcp23017 reset pin as output
    PORTA.DIR |= (1 << PIN1);

    rc = twi_master_init(&_twi, &TWIC, TWI_SLAVE_INTLVL_HI_gc, F_CPU, 100000);
    _twi_log_error("TWI master init", rc);
    if (rc)
        goto _leds_init_err;

    rc = mcp23017_init(&_led_expander, 0x00);
    _mcp23017_log_error("MCP23017 init", rc);
    if (rc)
        goto _leds_init_err;

    rc = mcp23017_set_direction_for_all(&_led_expander, 0x0000);
    _mcp23017_log_error("MCP23017 setdirection for all", rc);
    if (rc)
        goto _leds_init_err;

    rc = mcp23017_set_value_for_all(&_led_expander, 0x000);
    _mcp23017_log_error("MCP23017 set value", rc);

    _leds.worker_func[leds_sequence_init] = _sequence_init_worker;
    _leds.worker_func[leds_sequence_manual] = _sequence_manual_worker;
    _leds.worker_func[leds_sequence_foo] = _sequence_foo_worker;
    _leds.worker_func[leds_sequence_bar] = _sequence_bar_worker;
    _leds.worker_func[leds_sequence_baz] = _sequence_baz_worker;
    _leds.worker_func[leds_sequence_qux] = _sequence_fck_worker;

    debug_command_register(&_cmd_set_led);

_leds_init_err:
    return rc;
}

uint8_t leds_set_sequence(leds_sequence_t sequence) {
    if (sequence >= _leds_sequence_size)
        return 1;

    _leds.current_worker = sequence;

    return 0;
}

uint8_t leds_manual_set_value(uint16_t value) {
    if (_leds.current_worker != leds_sequence_manual)
        return 1;

    _leds.value = value;

    return 0;
}

uint8_t leds_worker(void) {
    uint16_t now = uptime();
    uint8_t rc = 0;

    if (_leds.timer_event) {
        _leds.time_event_cnt++;

        rc = _leds.worker_func[_leds.current_worker]();
        if (rc)
            return rc;

        _leds.timer_event = 0;
    }

    if (_leds.last_worker_run != now) {
        // LOG_DEBUG("Time events freq %u te/s", _leds.time_event_cnt / (now - _leds.last_worker_run));

        _leds.last_worker_run = now;
        _leds.time_event_cnt = 0;
    }

    return rc;
}

const char *leds_sequence_name(void) {
    static char *sequence_name[] = {"init", "manual", "foo", "bar", "baz", "qux"};

    if (_leds.current_worker >= ARRAY_SIZE(sequence_name))
        return "undefined";

    return sequence_name[_leds.current_worker];
}

static uint8_t _mcp23017_reset_pin_set(uint8_t value) {
    if (value)
        PORTA.OUT |= (1 << PIN1);
    else
        PORTA.OUT &= ~(1 << PIN1);

    return 0;
}

static uint8_t _mcp23017_i2c_write_data(uint8_t address, uint8_t *data, uint8_t size) {
    uint8_t rc = 0;

    rc = twi_master_write(&_twi, address, data, size);
    if (rc)
        return rc;

    while (_twi.status != twi_status_READY)
        ;

    return 0;
}

static uint8_t _mcp23017_delay_ms(uint32_t ms) {
    while (--ms) {
        _delay_ms(1);
    }
    return 0;
}

static debug_cmd_error_t _on_cmd_led_set(int argc, char *argv[]) {
    if (argc < 2)
        return debug_cmd_error_ARGERR;

    int sequence = atoi(argv[1]);

    if (sequence < 0 || sequence >= _leds_sequence_size)
        return debug_cmd_error_ARGERR;

    uint8_t rc = leds_set_sequence((leds_sequence_t)sequence);

    if (rc)
        return debug_cmd_error_FAIL;

    return debug_cmd_error_SUCCESS;
}

void leds_timer_interrupt_handler(void) { _leds.timer_event = 1; }

ISR(TWIC_TWIM_vect) { twi_master_isr_handler(&_twi); }
