# led-heart

## Pull submodules

```bash
git submodule update --init --recursive
```

## Building

```bash

# Specify path to gcc-avr if other than ${PATH}
export GCC_AVR_PATH='/path/to/gcc-avr'

# To turn on debug build
export AVR_LOG='debug'

make
```

## Hardware

PCB project can be found [here](https://gitlab.com/ru-kicad/led_heart)
